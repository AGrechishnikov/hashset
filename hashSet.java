import java.util.Iterator;
import java.util.NoSuchElementException;
import java.lang.Math;
public class hashSet<T> {

    private  Node<T> hashTable[]=new Node[10];
    private int tableSize=0;

    private int hashCode(T item){
        return Math.abs(item.hashCode()) % hashTable.length;
    }

    public int size(){
        return tableSize;
    }

    hashSet(){
        for(int i=0;i<hashTable.length;i++){
            hashTable[i]=null;
        }
    }

    public boolean isEmpty(){
        for(int i=0;i<hashTable.length;i++){
            if(hashTable[i]!=null){
                return false;
            }
        }
        return true;
    }

    public boolean contains(T item){
        int index = hashCode(item);
        Node temporary = hashTable[index];
        while(temporary!=null){
            if(temporary.data==item){
                return true;
            }
            temporary=temporary.next;
        }
        return false;
    }

    public void add(T item){
        int index = hashCode(item);
        Node temporary = hashTable[index];
        if(!contains(item)){
            if(temporary==null){
                hashTable[index]=new Node(item);
            }
            while(temporary!=null){
                temporary=temporary.next;
            }
            temporary=new Node(item);
            tableSize++;
        }
    }

    public void remove(T item){
        int index = hashCode(item);
        Node temporary = hashTable[index];
        if(hashTable[index]==null) throw new NoSuchElementException();
        if(hashTable[index].data==item){
            hashTable[index]=temporary.next;
            tableSize--;
            return;
        }

        Node previous = temporary;
        while(temporary!=null){
            if(temporary.data==item){
                previous.next=temporary.next;
                tableSize--;
                return;
            }
            previous=temporary;
            temporary=temporary.next;
        }
    }

    public void printAll(){
        for(int i=0;i<hashTable.length;i++){
            if(hashTable[i]!=null){
                Node temporary=hashTable[i];
                while(temporary!=null){
                    System.out.println(temporary.data);
                    temporary=temporary.next;
                }
            }
        }
    }



}
