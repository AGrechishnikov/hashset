public class Node<T> {
    T data;
    Node next;

    int nodeIndex=0;
    Node(T data) {
        this.data = data;
        this.next = null;

    }
}
